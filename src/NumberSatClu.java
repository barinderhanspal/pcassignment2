//******************************************************************************
//
// File:    NumberSatSmp.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import edu.rit.pj2.Job;
import edu.rit.pj2.LongVbl;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Node;
import edu.rit.pj2.Rule;
import edu.rit.pj2.Task;
import edu.rit.pj2.TaskSpec;
import edu.rit.pj2.Tuple;
import edu.rit.util.LongRange;

/**
 * Class NumberSatClu is a multi-node. multicore parallel program that solves the 
 * Number-SAT or #-SAT problem. In the Number-SAT/#-SAT problem, we determine to
 * number of satisfying assignments for a given Boolean expression.
 * 
 * This boolean expression/formula is represented in CNF(conjunctive normal form)
 * which consists of <i>clauses</i> and <i>literals</i>. This program calculates
 * the number of satisfying assignments for a given input boolean expression
 * using a brute-force approach. The program splits the calculation work by 
 * dividing the problem across the number of nodes (K) specified at in the input
 * to this program and uses a parallelFor loop on those nodes present in the cluster.
 * The parallelFor loop running on each of the worker nodes use a pattern where threads
 * are schedules using the leapfrog method. Each thread solves a unique iteration
 * of the for loop, dividing the work across different threads. The worker node 
 * keeps track of the number of satisfying assignments for the input CNF
 * expression and after all the parallel threads have finished their task, they
 * put a result tuple into the tuple space that contains the count of the number
 * of satisfying assignments. The Job notices picks up the result tuples from 
 * the tuple space near the end of the job's execution, does a reduction on these
 * tuples and prints the value of num sat counter at the job's console.
 * 
 * <P>
 * Usage: <TT>java pj2 jar=<jar_file> NumberSatClu <I>input_file_name<I> K</TT>
 * <P>
 * The program reads the input from the 'input_file_name' argument. The input 
 * must comply to the "DIMACS format". The program currently supports only 
 * variables in the range of [-63,63] excluding '0'(Zero). Also, the
 * number of clauses should be greater than or equal to 1.
 * The letter K signifies the number of worker tasks that would work together in
 * the cluster to solve the problem.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 *
 */
public class NumberSatClu extends Job {

	ArrayList<Long> clauses; // Array to store each of the clauses.

	/*
	 * Stores the flipping bitset information for working with NEGATIVE literals
	 * This flipping mask stores the bitset flipping(XOR operation) information 
	 * required to work with negative literals.
	 */
	ArrayList<Long> flippingMasks; 

	long numOfLiterals; // Number of Literals (V)
	long numOfClauses;  // Number of Clauses  (C)

	/**
	 * Constructor to initialize the class variables. Integer variables 
	 * numOfLiterals and numOfClauses are set to a default value of -1.
	 * numSatCount variable is set to 0. Other variables are created using
	 *  the 'new' keyword.
	 */
	public NumberSatClu() {

		numOfLiterals = -1;
		numOfClauses = -1;
		clauses = new ArrayList<Long>();
		flippingMasks = new ArrayList<Long>();
	}
	
	/**
	 * Tuple that contains the range of iterations for the worker tasks to compute
	 * 
	 * @author BarinderSingh
	 */
	private static class ChunkTuple extends Tuple{
		
		public LongRange range; // range of chunks
		
		/**
		 * Default Constructor
		 */
		public ChunkTuple(){}
		
		/**
		 * Constructor with parameters to set the class variable
		 * 
		 * @param range contains the range value for the chunk
		 */
		public ChunkTuple(LongRange range){
			
			this.range = range;
		}
	}

	/**
	 * Tuple containing the input CNF Expression tuple. This tuple is sent
	 * from the Job main program to all worker tasks. 
	 * 
	 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
	 *
	 */
	private static class CNFExpression
	extends Tuple  {

		public ArrayList<Long> clauses;
		public ArrayList<Long> flippingMasks;

		long numOfLiterals; // Number of Literals (V)

		/**
		 * Default Constuctor
		 */
		public CNFExpression(){}

		/**
		 * Constructor with parameters
		 * 
		 * @param clauses Arraylist containing the clauses
		 * @param flippingMasks Arraylist containing the flipping masks
		 * @param numOfLiterals long variable containing the value of number of
		 * 						literals.
		 */
		public CNFExpression(ArrayList<Long> clauses, ArrayList<Long> flippingMasks,
				long numOfLiterals){
			this.clauses = clauses;
			this.flippingMasks = flippingMasks;
			this.numOfLiterals = numOfLiterals;
		}
	}

	/**
	 * Job's main program
	 */
	public void main(String[] args) throws Exception {

		if(args.length != 2)
			usage();

		int K = 1;

		try{
			K = Integer.parseInt(args[1]);
		}catch(Exception e){
			System.err.println(e.getMessage());
			usage();
		}

		File inputFile = new File(args[0]);

		// Extract the CNF expression from the input file
		extractCnfExpression(inputFile);

		putTuple (K, new CNFExpression(clauses,flippingMasks, numOfLiterals)); 
		
		rule(new Rule()
				.task(K, new TaskSpec(NumberSatWorkerTask.class)
					.requires(new Node()
						.cores(Node.ALL_CORES))));

		rule(new Rule()
				.atFinish()
					.task(new TaskSpec(ReduceTask.class)
						.args(""+K)
							.runInJobProcess(true)));

	}

	/**
	 * Print a usage message and exit program when invalid input arguments
	 * are provided.
	 * 
	 * @throws IllegalArgumentException throws an illegal argument expression exception
	 * 									when number of args do not match requirements
	 */
	private void usage(){
		System.err.println("Usage: java pj2 NumberSatSmp <input_file> <K>");
		System.out.println("<input_file> File containing the CNF expression");
		System.out.println("<K> Number of Worker Threads");
		throw new IllegalArgumentException();
	}


	/**
	 * This function is used to extract the given CNF expression from the input file.
	 * The CNF expression must be follow the "DIMACS format" for SAT solvers. The
	 * function uses Scanners on the input file to extract the clauses and literals.
	 * The program also checks if the value for number of literals (V) lies between
	 * the range [1,63] and also if the number of clauses (C) has a value greater
	 * than 1. A check is also performed on the values of literals to ensure that
	 * they meet the range requirements(positive and negative)
	 * 
	 * @param inputFile the file from which the input CNF expression is to be 
	 * 					extracted.
	 * @throws Exception throws Exception when input does not comply to rules.
	 */
	private void extractCnfExpression(File inputFile) throws Exception{	

		Scanner fileScanner = new Scanner(inputFile);

		/*
		 * The flipping mask contains bitsets with information about which of the index
		 * of the assignments needs its values to be flipped (to represent a negation operation).
		 * Flipping masks for each and every clause are stored in its corresponding clause index
		 * of flippingMasks arraylist. 
		 */
		long flippingMask = 0L;
		long clause = 0L;

		while(fileScanner.hasNext()){

			if(fileScanner.hasNextLong()){

				long readValue = fileScanner.nextLong();

				if(numOfLiterals == -1){

					// If numOfLiterals variable is not initialized with data from file

					if(readValue < 1) {

						/*
						 *  If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be less than 1.");

					} else if(readValue > 63) {

						/*
						 *  If value read from file is greater than 63, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be greater than 63.");
					}

					// Valid input value
					numOfLiterals = readValue;

				} else if (numOfClauses == -1){

					// If numOfClauses variable is not initialized with data from file

					if(readValue < 1){

						/*
						 * If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of clauses(C) cannot be less than 1");
					}

					// Valid input value
					numOfClauses = readValue;

				} else {

					// Read Clauses and Literals

					if(Math.abs(readValue) > numOfLiterals){

						/*
						 * If literal value exceeds the max literal value mentioned in input 
						 * configuration close the fileScanner to avoid resource leaks and 
						 * throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("One or more clauses in the input CNF expression"
								+" have a value of literal(V) greater than defined value");

					} else if(readValue == 0){

						/*
						 * If a value of '0' is read, signifying end of clause, add the literals
						 * read till this point into the array list storing the clauses. Also, add
						 * the corresponding flipping masks to handle negative literal values
						 */
						clauses.add(clause);
						flippingMasks.add(flippingMask);

						// Resetting the flipping mask and clause long integer bitset variables
						flippingMask = 0L;
						clause = 0L;

					} else {

						if(readValue < 0){

							/* 
							 * Set flipping bit in the flippingMask long integer bitset variable
							 * for the negative literal value.
							 */
							flippingMask |= 1L << (Math.abs(readValue) - 1);
						}

						clause |= 1L << (Math.abs(readValue) - 1);
					}
				}

			} else {

				// Skip characters(non Long values)
				fileScanner.next();
			}
		}

		fileScanner.close();

		if(numOfClauses != clauses.size()){

			/*
			 * If number of clauses read in the expression do not match the declared value
			 * in the input configuration, throw an exception indicating invalid input
			 */
			throw new Exception("Number of clauses(C) in the CNF expression read from file does not match the defined value");
		}
	}

	
	/**
	 * The NumberSatWorkerTask is the Worker task class. This worker task is 
	 * run on all K node of Cluster of nodes. It utilizes the power of parallelFor
	 * loop to calculate the NumSAT in parallel and puts the result tuple that
	 * contains the numSatCount into the tuple space when the program tries out
	 * all the assignments.
	 * 
	 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
	 *
	 */
	public static class NumberSatWorkerTask extends Task{

		ArrayList<Long> clauses; // Array to store each of the clauses.

		/*
		 * Stores the flipping bitset information for working with NEGATIVE literals
		 * This flipping mask stores the bitset flipping(XOR operation) information 
		 * required to work with negative literals.
		 */
		ArrayList<Long> flippingMasks; 

		long numOfLiterals; // Number of Literals (V)
		long lowerBound, upperBound;
		LongVbl numSatCount;
		CNFExpression aCNF;
		
		/**
		 * Default Constructor: Sets the initial value of NumSatCount variable
		 */
		public NumberSatWorkerTask(){
			
			numSatCount = new LongVbl.Sum(0);
		}
		
		/**
		 * This function is used to set the NumSatWorkerTask's Cnf expression
		 * variable with the value supplied as the argument.
		 * 
		 * @param aCNF the value that is to be copied into the class variable.
		 */
		public void setCNFExpression(CNFExpression aCNF){

			this.clauses = aCNF.clauses;
			this.flippingMasks = aCNF.flippingMasks;
			this.numOfLiterals = aCNF.numOfLiterals;
			lowerBound = 0L;
			upperBound = 0L;
		}
		
		/**
		 * Worker Thread's main program
		 */
		public void main(String[] args) throws Exception {

			// Set the class variables with data from tuple space
			setCNFExpression((CNFExpression) takeTuple (new CNFExpression()));

			// Calculate NumSat for the CNF
			calculateNumSat();
			
		}
		
		/**
		 * The calculateNumSAT function, calculates the number of satisfying 
		 * assignments for the CNF expression read from the input file. The 
		 * function uses brute-force to calculate the solution. A parallelFor loop 
		 * schedules multiple threads using the leapfrog method and this divides work
		 * across multiple threads. These threads collectively iterates over all the 
		 * subsets of elements 0 (zero) through ('numOfLiterals' - 1) and for each 
		 * of the subsets, the values of respective bit indexes are substituted into
		 * the respective literals in the input CNF expression. If the complete CNF
		 * expression is satisfied (returns 'true'), the counter variable 
		 * (numSatThreadCount) is incremented by one. 'numSatThreadCount' is the
		 * thread local variable for the numSatCount variable. The parallelFor loop
		 * implementation in the PJ2 library handles updation of 'numSatCount' variable
		 * for the values set in the numSatThreadCount variable by each and every thread.
		 * After the value of numSatCounter has been calculated by the the worker
		 * thread, it puts the result tuple into the tuple space for the Job to reduce
		 * 
		 * @throws Exception 
		 */
		public void calculateNumSat() throws Exception{
			
			// The max/last value of the assignment
			long maxAssignment = (1L << numOfLiterals) - 1L;
			
			// Slicing the assignments into K(no. of workers) equal parts
			LongRange slice = new LongRange(0L, maxAssignment)
									.subrange(groupSize(), taskRank());
			
			lowerBound = slice.lb();
			upperBound = slice.ub();
			
				/*
				 * Parallel for loop running through range [lower bound, upperbound].
				 * Threads generated by the parallelFor implementation are scheduled
				 * in a leapfrog method.
				 */
				parallelFor (lowerBound, upperBound).schedule(leapfrog).exec(new Loop(){

					boolean cnfValueForCurrentSubset;
					LongVbl numSatThreadCount;

					public void start() throws Exception{
						/*
						 * numSatThreadCount is the thread local variable for numSatCount.
						 * Updating the numSatCount variable using the thread local variable
						 * is handled automatically by the PJ2 library
						 */
						numSatThreadCount = (LongVbl) threadLocal (numSatCount);
					}

					public void run(long assignmentIndex){

						// Initializing the cnfValueForCurrentSubset variable to 'true'
						cnfValueForCurrentSubset = true;

						for(int cnfClauseIndex = 0; cnfClauseIndex < clauses.size(); cnfClauseIndex ++){
							/*
							 * A clause value for the current cnf expressiom is calculated as:
							 * 1. XOR/flip the assignmentIndex long integer bitset using the flipping mask
							 * 		for the given clause.
							 * 2. Check if the long integer bitset value exists in the result from step 1.
							 */
							boolean clauseValueForCurrentSubset =	( ((assignmentIndex ^ flippingMasks.get(cnfClauseIndex)) & clauses.get(cnfClauseIndex) ) != 0L);

							if(clauseValueForCurrentSubset == false){

								// If a clause value is calculated as 'false', set cnf value as 'false' and break.
								cnfValueForCurrentSubset = false;
								break;					
							}

							// Perform an AND operation to calulate result of conjunction of two or more clauses.
							cnfValueForCurrentSubset &= clauseValueForCurrentSubset;
						}

						if(cnfValueForCurrentSubset == true){
							/*
							 *  If the CNF expression for the given subset assignment evaluates to true
							 *  increment numSatCount
							 */
							++ numSatThreadCount.item;
						}
					}
				});

				// Put the value of calculated NumSAT for input CNF expression into tuple space
				putTuple(new ResultTuple (numSatCount.item));
		}
	}

	/**
	 * The Result class provides the print tuple to print the numSatCounter
	 * results onto the console.
	 * 
	 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
	 *
	 */
	private static class ResultTuple
	extends Tuple{

		private long numSatCount;

		/**
		 * Constructor with parameters to set the class variable
		 * 
		 * @param numSatCount the value that is to be copied into the class variable
		 */
		public ResultTuple(long numSatCount){
			
			this.numSatCount = numSatCount;
		}
	}

	/**
	 * The ReduceTask class combines the results calculated by the worker tasks
	 * and prints the overall result onto the Job's console 
	 * @author BarinderSingh
	 *
	 */
	public static class ReduceTask extends Task{

		/**
		 * ReduceTask's main function
		 */
		public void main(String[] args) throws Exception {

			int K = Integer.parseInt(args[0]);
			long overallNumSatCount = 0;

			// Add numSatCount values calculated by K worker tasks
			for(int index=0; index < K; index ++)
				overallNumSatCount += ((ResultTuple) getTuple(index)).numSatCount;
				
			System.out.println(overallNumSatCount);
		}
	}
}
