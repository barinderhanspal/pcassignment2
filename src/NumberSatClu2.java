import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.rit.pj2.Job;
import edu.rit.pj2.LongVbl;
import edu.rit.pj2.Loop;
import edu.rit.pj2.Node;
import edu.rit.pj2.Print;
import edu.rit.pj2.Rule;
import edu.rit.pj2.Section;
import edu.rit.pj2.Task;
import edu.rit.pj2.TaskSpec;
import edu.rit.pj2.Tuple;
import edu.rit.pj2.Vbl;

/**
 * 
 */

/**
 * @author BarinderSingh
 *
 */
public class NumberSatClu2 extends Job {

	private static class CNFExpression
	extends Tuple 
	implements Vbl {

		public ArrayList<Long> clauses;
		public ArrayList<Long> flippingMasks;

		long numOfLiterals; // Number of Literals (V)

		public CNFExpression(){

		}

		public CNFExpression(ArrayList<Long> clauses, ArrayList<Long> flippingMasks,
				long numOfLiterals){
			this.clauses = clauses;
			this.flippingMasks = flippingMasks;
			this.numOfLiterals = numOfLiterals;
		}

		public void set(Vbl vbl) {
			CNFExpression aCNF = (CNFExpression) vbl;
			this.clauses = aCNF.clauses;
			this.flippingMasks = aCNF.flippingMasks;
			this.numOfLiterals = aCNF.numOfLiterals;
		}

		@Override
		public void reduce(Vbl arg0) {
			// TODO Auto-generated method stub

		}
	}

	ArrayList<Long> clauses; // Array to store each of the clauses.

	/*
	 * Stores the flipping bitset information for working with NEGATIVE literals
	 * This flipping mask stores the bitset flipping(XOR operation) information 
	 * required to work with negative literals.
	 */
	ArrayList<Long> flippingMasks; 

	long numOfLiterals; // Number of Literals (V)
	long numOfClauses;  // Number of Clauses  (C)

	/**
	 * Constructor to initialize the class variables. Integer variables 
	 * numOfLiterals and numOfClauses are set to a default value of -1.
	 * numSatCount variable is set to 0. Other variables are created using
	 *  the 'new' keyword.
	 */
	public NumberSatClu2() {
		
		numOfLiterals = -1;
		numOfClauses = -1;

		clauses = new ArrayList<Long>();
		flippingMasks = new ArrayList<Long>();
	}

	/**
	 * Print a usage message and exit program when invalid input arguments
	 * are provided.
	 * 
	 * @throws IllegalArgumentException throws an illegal argument expression exception
	 * 									when number of args do not match requirements
	 */
	private void usage(){
		System.err.println("Usage: java pj2 NumberSatSmp <input_file> <K>");
		System.out.println("<input_file> File containing the CNF expression");
		System.out.println("<K> Number of Worker Threads");
		throw new IllegalArgumentException();
	}

	
	/**
	 * This function is used to extract the given CNF expression from the input file.
	 * The CNF expression must be follow the "DIMACS format" for SAT solvers. The
	 * function uses Scanners on the input file to extract the clauses and literals.
	 * The program also checks if the value for number of literals (V) lies between
	 * the range [1,63] and also if the number of clauses (C) has a value greater
	 * than 1. A check is also performed on the values of literals to ensure that
	 * they meet the range requirements(positive and negative)
	 * 
	 * @param inputFile the file from which the input CNF expression is to be 
	 * 					extracted.
	 * @throws Exception throws Exception when input does not comply to rules.
	 */
	private void extractCnfExpression(File inputFile) throws Exception{	

		Scanner fileScanner = new Scanner(inputFile);
		
		/*
		 * The flipping mask contains bitsets with information about which of the index
		 * of the assignments needs its values to be flipped (to represent a negation operation).
		 * Flipping masks for each and every clause are stored in its corresponding clause index
		 * of flippingMasks arraylist. 
		 */
		long flippingMask = 0L;
		long clause = 0L;
					
		while(fileScanner.hasNext()){

			if(fileScanner.hasNextLong()){

				long readValue = fileScanner.nextLong();

				if(numOfLiterals == -1){

					// If numOfLiterals variable is not initialized with data from file
					
					if(readValue < 1) {

						/*
						 *  If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be less than 1.");

					} else if(readValue > 63) {

						/*
						 *  If value read from file is greater than 63, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be greater than 63.");
					}

					// Valid input value
					numOfLiterals = readValue;

				} else if (numOfClauses == -1){

					// If numOfClauses variable is not initialized with data from file
					
					if(readValue < 1){

						/*
						 * If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of clauses(C) cannot be less than 1");
					}

					// Valid input value
					numOfClauses = readValue;

				} else {

					// Read Clauses and Literals

					if(Math.abs(readValue) > numOfLiterals){
						
						/*
						 * If literal value exceeds the max literal value mentioned in input 
						 * configuration close the fileScanner to avoid resource leaks and 
						 * throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("One or more clauses in the input CNF expression"
								+" have a value of literal(V) greater than defined value");
						
					} else if(readValue == 0){
						
						/*
						 * If a value of '0' is read, signifying end of clause, add the literals
						 * read till this point into the array list storing the clauses. Also, add
						 * the corresponding flipping masks to handle negative literal values
						 */
						clauses.add(clause);
						flippingMasks.add(flippingMask);
						
						// Resetting the flipping mask and clause long integer bitset variables
						flippingMask = 0L;
						clause = 0L;
					
					} else {

						if(readValue < 0){
							
							/* 
							 * Set flipping bit in the flippingMask long integer bitset variable
							 * for the negative literal value.
							 */
							flippingMask |= 1L << (Math.abs(readValue) - 1);
						}
						
						clause |= 1L << (Math.abs(readValue) - 1);
					}
				}
				
			} else {
				
				// Skip characters(non Long values)
				fileScanner.next();
			}
		}
		
		fileScanner.close();
		
		if(numOfClauses != clauses.size()){
			
			/*
			 * If number of clauses read in the expression do not match the declared value
			 * in the input configuration, throw an exception indicating invalid input
			 */
			throw new Exception("Number of clauses(C) in the CNF expression read from file does not match the defined value");
		}

	}
	
	public void main(String[] args) throws Exception {

		if(args.length != 2)
			usage();

		int K = 1;
		
		try{
			K = Integer.parseInt(args[1]);
		}catch(Exception e){
			System.err.println(e.getMessage());
			usage();
		}
		
		File inputFile = new File(args[0]);

		// Extract the CNF expression from the input file
		extractCnfExpression(inputFile);

		putTuple (new CNFExpression(clauses,flippingMasks, numOfLiterals)); 
		
		rule(new Rule()
				.task(K, new TaskSpec(NumberSatWorkerTask.class)/*.requires(new Node().cores(Node.ALL_CORES))*/));
		
		rule(new Print.Rule());

	}
	
	
	
	public static class NumberSatWorkerTask extends Task{

		ArrayList<Long> clauses; // Array to store each of the clauses.

		/*
		 * Stores the flipping bitset information for working with NEGATIVE literals
		 * This flipping mask stores the bitset flipping(XOR operation) information 
		 * required to work with negative literals.
		 */
		ArrayList<Long> flippingMasks; 

		long numOfLiterals; // Number of Literals (V)

		LongVbl numSatCount = new LongVbl.Sum(0);
		
		int size;
		int rank;
		
		public void main(String[] arg0) throws Exception {
			
			CNFExpression aCNF = (CNFExpression) readTuple (new CNFExpression());
			
			this.clauses = aCNF.clauses;
			this.flippingMasks = aCNF.flippingMasks;
			this.numOfLiterals = aCNF.numOfLiterals;
			
			size = groupSize();
			rank = taskRank();
			
			// Calculating the last subset for the given value of number of literals
			final long maxAssignment = (1L << numOfLiterals) - 1L;

			parallelDo (new Section(){
				
				public void run() throws Exception{
					
					parallelFor (0L , maxAssignment).schedule(leapfrog).exec(new Loop(){

						boolean cnfValueForCurrentSubset;
						LongVbl numSatThreadCount;

						public void start() throws Exception{
							/*
							 * numSatThreadCount is the thread local variable for numSatCount.
							 * Updating the numSatCount variable using the thread local variable
							 * is handled automatically by the PJ2 library
							 */
							numSatThreadCount = (LongVbl) threadLocal (numSatCount);
						}

						public void run(long assignmentIndex){

							// Initializing the cnfValueForCurrentSubset variable to 'true'
							cnfValueForCurrentSubset = true;
							
							for(int cnfClauseIndex = 0; cnfClauseIndex < clauses.size(); cnfClauseIndex ++){
								/*
								 * A clause value for the current cnf expressiom is calculated as:
								 * 1. XOR/flip the assignmentIndex long integer bitset using the flipping mask
								 * 		for the given clause.
								 * 2. Check if the long integer bitset value exists in the result from step 1.
								 */
								boolean clauseValueForCurrentSubset =	( ((assignmentIndex ^ flippingMasks.get(cnfClauseIndex)) & clauses.get(cnfClauseIndex) ) != 0L);
								
								if(clauseValueForCurrentSubset == false){
									
									// If a clause value is calculated as 'false', set cnf value as 'false' and break.
									cnfValueForCurrentSubset = false;
									break;					
								}
								
								// Perform an AND operation to calulate result of conjunction of two or more clauses.
								cnfValueForCurrentSubset &= clauseValueForCurrentSubset;
							}
							
							if(cnfValueForCurrentSubset == true){
								/*
								 *  If the CNF expression for the given subset assignment evaluates to true
								 *  increment numSatCount
								 */
								++ numSatThreadCount.item;
							}
						}
					});
				}
			}/*,
			new Section(){
				public void run() throws Exception{
					
				}
			}*/);
			
			// Return value of calculated NumSAT for input CNF expression
			
			putTuple(new Result (numSatCount.item));
		}
	}
		
		
		/**
		 * The calculateNumSAT function, calculates the number of satisfying 
		 * assignments for the CNF expression read from the input file. The 
		 * function uses brute-force to calculate the solution. A parallelFor loop 
		 * schedules multiple threads using the leapfrog method and this divides work
		 * across multiple threads. These threads collectively iterates over all the 
		 * subsets of elements 0 (zero) through ('numOfLiterals' - 1) and for each 
		 * of the subsets, the values of respective bit indexes are substituted into
		 * the respective literals in the input CNF expression. If the complete CNF
		 * expression is satisfied (returns 'true'), the counter variable 
		 * (numSatThreadCount) is incremented by one. 'numSatThreadCount' is the
		 * thread local variable for the numSatCount variable. The parallelFor loop
		 * implementation in the PJ2 library handles updation of 'numSatCount' variable
		 * for the values set in the numSatThreadCount variable by each and every thread.
		 * 
		 * @return LongVbl value representing the number of satisfying assignments
		 * 			for the CNF expression read from the input file.
		 * @throws IOException 
		 */
		/*private void calculateNumSAT(CNFExpression aCNF) throws IOException {

			numSatCount = new LongVbl.Sum(0);
			this.clauses = aCNF.clauses;
			this.flippingMasks = aCNF.flippingMasks;
			this.numOfLiterals = aCNF.numOfLiterals;
			
			// Calculating the last/max subset for the given value of number of literals
			long maxAssignment = (1L << numOfLiterals) - 1L;
			
			 * Parallel for loop running through range [0,maxAssignment].
			 * Threads generated by the parallelFor implementation are scheduled
			 * in a leapfrog method.
			 
			parallelFor (0L , maxAssignment).schedule(leapfrog).exec(new Loop(){

				boolean cnfValueForCurrentSubset;
				LongVbl numSatThreadCount;

				public void start() throws Exception{
					
					 * numSatThreadCount is the thread local variable for numSatCount.
					 * Updating the numSatCount variable using the thread local variable
					 * is handled automatically by the PJ2 library
					 
					numSatThreadCount = (LongVbl) threadLocal (numSatCount);
				}

				public void run(long assignmentIndex){

					// Initializing the cnfValueForCurrentSubset variable to 'true'
					cnfValueForCurrentSubset = true;
					
					for(int cnfClauseIndex = 0; cnfClauseIndex < clauses.size(); cnfClauseIndex ++){
						
						 * A clause value for the current cnf expressiom is calculated as:
						 * 1. XOR/flip the assignmentIndex long integer bitset using the flipping mask
						 * 		for the given clause.
						 * 2. Check if the long integer bitset value exists in the result from step 1.
						 
						boolean clauseValueForCurrentSubset =	( ((assignmentIndex ^ flippingMasks.get(cnfClauseIndex)) & clauses.get(cnfClauseIndex) ) != 0L);
						
						if(clauseValueForCurrentSubset == false){
							
							// If a clause value is calculated as 'false', set cnf value as 'false' and break.
							cnfValueForCurrentSubset = false;
							break;					
						}
						
						// Perform an AND operation to calulate result of conjunction of two or more clauses.
						cnfValueForCurrentSubset &= clauseValueForCurrentSubset;
					}
					
					if(cnfValueForCurrentSubset == true){
						
						 *  If the CNF expression for the given subset assignment evaluates to true
						 *  increment numSatCount
						 
						++ numSatThreadCount.item;
					}
				}
			});

			// Return value of calculated NumSAT for input CNF expression
			putTuple(new Result (numSatCount.item));
		}

	}
	*/
	private static class Result
	extends Print.Tuple{

		private long numSatCount;

		public Result(long numSatCount){
			this.numSatCount = numSatCount;
		}

		public void print(){
			synchronized (System.out) {
				System.out.println(numSatCount);
			}
		}
	}

}
